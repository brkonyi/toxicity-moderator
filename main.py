import praw
import requests

TOXICITY_THRESHOLD = 0.75

reddit = praw.Reddit('Toxicity-Moderator')
subreddit = reddit.subreddit('MorbidReality')

for comment in subreddit.stream.comments():
    request = {
            "comment": {"text": comment.body},
            "languages": ["en"],
            "requestedAttributes": {
                "TOXICITY":{}
            }
        }
    resp = requests.post('https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze?key=NO_KEY_FOR_YOU',
            json=request);

    response = resp.json()
    scores = response['attributeScores']
    toxicityAttrib = scores['TOXICITY']
    toxicitySummary = toxicityAttrib['summaryScore']
    toxicityProbability = float(toxicitySummary['value'])
    if toxicityProbability > TOXICITY_THRESHOLD:
        print "Reporting toxic comment made by: " + comment.author.name
        print "Comment: " + comment.body
        print "Toxicity: " + str(toxicityProbability)
        comment.report("This comment has been deemed toxic by an experimental bot created by /u/XtremeCheese.")
        print ""
        print "------------------------------------"
        print "------------------------------------"
